/* SPDX-License-Identifier: MIT */
#include "util-munit.h"

int
main(int argc, char **argv)
{
	return munit_tests_run(argc, argv);
}
